# Pepper-vocal learning of gestures

--------------------------------------------------Groupe 12-------------------------------------------------------------------------

Abdelaziz Miyara - Manon Elkaim - Younes Barie  - Maxence Petitpierre

Link to youtube video: https://www.youtube.com/watch?v=qUiDnKnfT3s&feature=youtu.be

--------------------------------------------------PROJET PEPPER---------------------------------------------------------------------

le sujet permet def aire apprendre à un robot Pepper un mouvement ou une position. Il doit ensuite pouvoir sauvegarder ce qu'il vient d'apprendre
et pouvoir le refaire.
Pour cela nous avons opté pour 2 solutions :
- A l'aide du logiciel Choréhraphe : 
		ProjetMajeur G12S4 : Programme principal pour faire apprendre le mouvement
		vocal : programme test permettant de dialoguer les mots ok et abandon avec le robot pour reconnaissance de mots
- Par le biais d'un script python : peper.py qui est présenté dans ce GitLab

Pour pouvoir utiliser le script Python, il faut rentrer l'ip du Pepper au début du code. Il faut que le pc qui éxécute le script python
soit connecté au wifi du Pepper et il suffit de l'éxécuter.



the subject allows for a Pepper robot to learn a movement or position. He must then be able to save what he has just learned and be able to do it again.
For this reason we creaed 2 solutions:

-Using the Choreograph software:
    ProjectMajor G12S4: Main program to teach movement
    vocal: test program allowing to dialogue the words ok and abandon with the robot for word recognition
-By means of a python script: peper.py which is presented in this GitLab

To be able to use the Python script, you must enter the Pepper ip at the beginning of the code. It is necessary that the pc that runs the python script
is connected to the Pepper's wifi and just run it.



