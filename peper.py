# -*- coding: utf-8 -*-
"""
Created on Wed May 17 14:10:16 2017

@author: abdelaziz miyara
"""

#include <alproxies/altexttospeechproxy.h>

import qi
import argparse
import sys
import time
from naoqi import ALProxy
from xlwt import Workbook
import xlrd
import ast

######################## fonction d'initialisation ############################

def init():
    global motion_service
    global tts
    global asr_service
    global memProxy
    global animatedSpeechProxy
    global touch
    
    # Get the service ALTextToSpeech.
    tts = session.service("ALTextToSpeech")
    # Get the service ALMotion.
    motion_service  = session.service("ALMotion")
    # Get the service ALSpeechRecognition.
    asr_service = session.service("ALSpeechRecognition")
    touch = session.service("ALTouch") 
    memProxy = ALProxy("ALMemory", robotIP, 9559)
    animatedSpeechProxy = ALProxy("ALAnimatedSpeech", robotIP, 9559)
    
######################### Met la stifness à 1 #################################
    
def stiffnessOn():
    names = ["Head","RArm", "LArm", "Leg"]
    stiffnessLists = 1.0
    timeLists = 1.0
    for i in range(3):
        motion_service.stiffnessInterpolation(names[i], stiffnessLists, timeLists)
        
######################### Met la stifness à 0 #################################
        
def stiffnessOff():
    names = ["Head","RArm", "LArm", "Leg"]
    stiffnessLists = 0.0
    timeLists = 1.0
    for i in range(3):
        motion_service.stiffnessInterpolation(names[i], stiffnessLists, timeLists)
        
########################## Memorise un mouvement ##############################
        
def MemorizeMVT():
    global end
    end =""
    global L
    #names = ["Head","RArm", "LArm", "Leg","Wheels"]
    stiffnessOff()
    tts.say('Faites moi faire le mouvement et dites OK une fois terminé')
    useSensors  = True
    sensorAngles=[]
    i=0
    while end != "ok":
        print("ned",end)        
        print(i)
        i=i+1
        end=listen2()
    #for i in range(0, 100):
        sensorAngles.append(motion_service.getAngles("Body", useSensors))
    print("end",end)
    return sensorAngles
    
######################### Realise le mouvement ################################
    
def doMouvement(mouvement):
    global L
    print(L)
    tts.say('je bouge')
    print(mouvement)
    for i in range(0, L-1):
        mouvements = mouvement[i]
        fractionMaxSpeed = 0.3
        motion_service.setAngles("Body", mouvements, fractionMaxSpeed)
        #time.sleep(0.1)
    time.sleep(1)

    
###################### Touche la tete #########################################   
    
def touched():
    head=touch.getStatus()[9][1]
    if head==True:
        tts.say('tu m touche la tête')
    print(head)
    return head
    
########################fonction listen blocante###############################   
def listen():
    asr_service.pause(True)
    asr_service.setLanguage("French")
    
    vocabulary = ["ok","apprendre","jouer","oui","non","main","tête","droite","gauche","un","deux","trois","lever","baisser","touner"]
    asr_service.setVocabulary(vocabulary, False)
    word=[]
    i=0
    while i<1:
        #time.sleep(1)
        #Start the speech recognition engine with user 192.168.1.40
        asr_service.subscribe(robotIP)
        #tts.say('Speech recognition engine started')
        #time.sleep(3.0)
        memProxy.subscribeToEvent('WordRecognized',"192.168.1.40", 'wordRecognized')
        asr_service.pause(False)
        word=memProxy.getDataOnChange("WordRecognized",0)
        asr_service.unsubscribe(robotIP)
        asr_service.pause(True)
        #time.sleep(10.0)
        if word[1]>0.4: i=2 # probabilité du mot entendu superieur à 0.4
    print (word)
    return word[0]
    
########################fonction listen non blocante###########################
    
def listen2():
    asr_service.pause(True)
    asr_service.setLanguage("French")
    
    vocabulary = ["ok","apprendre","jouer","oui","main","tête","droite","gauche","un","deux","trois","lever","baisser","touner"]
    asr_service.setVocabulary(vocabulary, False)
    word=[]
    #time.sleep(1)
    #Start the speech recognition engine with user 192.168.1.40
    asr_service.subscribe(robotIP)
    #tts.say('Speech recognition engine started')
    #time.sleep(3.0)
    memProxy.subscribeToEvent('WordRecognized',"192.168.1.40", 'wordRecognized')
    asr_service.pause(False)
    word=memProxy.getData("WordRecognized",0)
    time.sleep(2.0)
    asr_service.unsubscribe(robotIP)
    asr_service.pause(True)


    return word[0]
    
################### ecrit sur le fichier xls ##################################

def DBenter():
    global answer
    global mouvement 
    global L
    global total_rows
    
    wb = Workbook()
    sheet1 = wb.add_sheet('Sheet 1')
    wb.save('DBpepper.xls')
    
    workbook = xlrd.open_workbook('DBpepper.xls')
    worksheet= workbook.sheet_by_name("Sheet 1")
    total_rows = worksheet.nrows
    print(total_rows)
    
    tts.say("tu as dit " + answer)
    print(answer)
    tts.say("donner un nom à ce mouvement")
    print("donner un nom à ce mouvement")
    tts.say("Pour celà suivez 4 etapes")
    print("Pour celà suivez 4 etapes")
    tts.say("premièrement decrivez le mouvement exemple lever baisser ou touner")
    print("premièrement decrivez le mouvement exemple lever baisser ou touner")
    name1=listen()
    print(name1)
    tts.say("deuxiemmement donner le nom du membre bougé: tête ou main ")
    print("deuxiemmement donner le nom du membre bougé: tête ou main ")
    name2=listen()
    print(name2)
    if name2 == "main":
        tts.say("maintenant choisissez droite ou gauche")
        print("maintenant choisissez droite ou gauche")
        name3=listen()
        print(name3)
    elif name2 == "tête":
        tts.say("tournez vous la tete vers la droite ou la gauche")
        print("tournez vous la tete vers la droite ou la gauche")
        name3=listen()
        print (name3)                 
    tts.say("finalement numerotez ce mouvement par exemple un deux ou trois ")
    print("finalement numerotez ce mouvement par exemple un deux ou trois ")
    name4=listen()
    print(name4)
    
    wb = Workbook()
    sheet1 = wb.add_sheet('Sheet 1')
    
    sheet1.write(total_rows,0,name1)
    sheet1.write(total_rows,1,name2)
    sheet1.write(total_rows,2,name3)
    sheet1.write(total_rows,3,name4)
    
    for i in range(0, L):
        sheet1.write(total_rows,i+4,str(mouvement[i]))
        sheet1.col(i).width = 10000   
    wb.save('DBpepper.xls')
    
############################## lire depuis un fichier xls #####################
 
def DBread():

    global angles
    global mvt
    global L
    #open the spreadsheet file(or workbook)
    global total_rows
    angles1=[]
    workbook = xlrd.open_workbook('DBpepper.xls')
    #open sheet:
    worksheet= workbook.sheet_by_name("Sheet 1")
    total_rows = worksheet.nrows
    total_cols = worksheet.ncols
    mvt=[]
    
    table =list()
    record = list()
    
    for x in range(total_rows):
        for y in range(total_cols):
            record.append(worksheet.cell(x,y).value)
        table.append(record)
        record = []
        x+=1
        print(table)
    #for i in range(0,3):  
    mvt=[str(table[0][0])+" "+str(table[0][1])+" "+str(table[0][2])+" "+str(table[0][3])]
    print(mvt)
    tts.say(str(mvt))

    for a in range(4,8):
        #for b in range(0,3):
        angles.append(table[0][a])
        angles=ast.literal_eval(angles[0])
        print(angles)
    return angles
    
########################## Fonction MAIN ######################################  
def main():
    # set the local configuration
    global configuration
    global answer
    global mouvement
    global angles
    global mvt
    global total_rows
    global commande
    global end
    global L
    
    touched()
    mouvement=[]
    configuration= {"bodyLanguageMode":"random"}
    # say the text with the local configuration
    motion_service.wakeUp()
    animatedSpeechProxy.say("Bonjour je m'appel Alfa le robot de C P E et vous pouvez m'apprendre des mouvements", configuration)
    print("Bonjour je m'appel Alfa le robot dominant et vous pouvez m'apprendre des mouvements")
    animatedSpeechProxy.say("Voulez vous me faire apprendre un mouvement ou me faire jouer un mouvement", configuration)
    print("Voulez vous me faire apprendre un mouvement ou me faire jouer un mouvement")
    
    while True:

        if touched()==True:
            motion_service.rest()
        commande=listen()
        print(commande)
        tts.say("tu as dit " + commande)
        if commande=="apprendre":
            #animatedSpeechProxy.say(" Faites-moi faire le mouvement et dites ok lorsque vous aurez fini ", configuration)
            mouvement=MemorizeMVT()
            L=len(mouvement)
            time.sleep(2.5)
            #end=listen()
            #tts.say("tu as dit " + end)
            if end =="ok":
                stiffnessOn()
                tts.say("je vais refaire le mouvement")
                print("je vais refaire le mouvement")
                doMouvement(mouvement)
                print("********************debut mouvement*******************")
                print (mouvement)
                print("**********************fin mouvement********************")
                #♦stiffnessOff()
                animatedSpeechProxy.say("Ce mouvement vous convient ?", configuration)
                print("Ce mouvement vous convient ?")
                #tts.say("Ce mouvement vous convient ?")
                answer=listen()
                print(answer)
                if answer == "oui":
                    DBenter()
                    tts.say("parfait")
                    print("parfait")
                    motion_service.rest()
                elif answer == "non":
                    tts.say("vous avez dit" + answer)
                    motion_service.rest()
                else:
                    tts.say("veuillez dire oui ou non")
                    print("veuillez dire oui ou non")
                    
        if commande =="jouer":
            angles = DBread()
            print("******************************debut angle******************")
            print (angles)
            print("*******************fin angle********************************")
            tts.say("voulez vous jouer le mouvement 1 2 ou 3")
            print("voulez vous jouer le mouvement 1 2 ou 3")
            number=listen()
            print(number)
            if number == "un":
                doMouvement(mouvement)
            #elif number == "deux":
            #    doMouvement(angles[1])
            #elif number == "trois":
            #    doMouvement(angles[2])
            tts.say("voulez vous combiner des mouvements?")
            print("voulez vous combiner des mouvements?")
            answer2=listen()
            if answer2 == "oui":
                tts.say("choisissez l'ordre des deux mouvements à combiner" )
                tts.say("donner le numeo du premier mouvement")
                #numero1=listen()
                tts.say("donner le numeo du deuxième mouvement")
                #numero2=listen()
                #doMouvement(angles[numero1])
                #time.sleep(1)
                #doMouvement(angles[numero2])
                
            elif answer2 == "non":
                motion_service.rest()

if __name__ == "__main__":
    robotIP="192.168.1.40"
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default=robotIP,
                        help="Robot IP address. On robot or Local Naoqi: use '192.168.1.40'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

    init()
    main()
    

